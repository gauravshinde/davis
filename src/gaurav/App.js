import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import "typeface-roboto";
import './App.css';
import { Provider } from 'react-redux';

import store from '../store/store';

import Navbar from "../gaurav/components/header/Navbar";

//Component
import PreMain from "../gaurav/components/pre-login/PreMain";
import TopMenu from './components/header/TopMenu';
import Mainmodal from "../gaurav/components/modalone/Mainmodal";
import MainModalTwo from '../gaurav/components/modaltwo/MainModalTwo';
import MainModalThree from '../gaurav/components/modalthree/MainModalThree';
import MainmodalFour from '../gaurav/components/modalfour/MainmodalFour';
import Events from '../gaurav/components/resources/Events';
import Association from '../gaurav/components/resources/Association';
import Bookmarks from '../gaurav/components/bookmarks/Bookmarks';
import Articles from '../gaurav/components/reports/Articles';
import MainLogin from './components/insidelogin/MainLogin';
import BlogMain from './components/insidelogin/blog/BlogMain';
import AllHeadingBox from './components/headingcomponent/AllHeadingBox';
import MainLoginNews from './components/LoginNews/MainLoginNews';

import Footer from '../gaurav/components/footer/Footer';
import MainDashboard from './components/dashboard/MainDashboard';



function App() {
  return (
    <Provider store={store}>
    <Router>
      <div>
        <Navbar/>
        <Route exact path="/" component={PreMain}/>
        <Route exact path="/maindashboard" component={MainDashboard} />
        <Route exact path="/TopMenu" component={TopMenu}/>
        <Route exact path="/Mainmodal" component={Mainmodal}/>
        <Route exact path="/MainModalTwo" component={MainModalTwo}/>
        <Route exact path="/MainModalThree" component={MainModalThree}/>
        <Route exact path="/MainmodalFour" component={MainmodalFour}/>
        <Route exact path="/Events" component={Events}/>
        <Route exact path="/Association" component={Association}/>
        <Route exact path="/Bookmarks" component={Bookmarks}/>
        <Route exact path="/Articles" component={Articles}/>
        <Route exact path="/MainLogin" component={MainLogin}/>
        <Route exact path="/BlogNews" component={BlogMain}/>
        <Route exact path="/AllHeading" component={AllHeadingBox}/>
        <Route exact path="/MainLoginNews" component={MainLoginNews}/>
        <Footer/>
      </div>
    </Router>
    </Provider>
  );
}

export default App;
