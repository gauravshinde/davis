import React, { Component } from 'react'

export class Default extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="d-flex ">
                            <h2>Page Not Found</h2>
                        </div>
                    </div>
                    
                </div>
            </React.Fragment>
        )
    }
}

export default Default
