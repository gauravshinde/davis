import React, { Component } from 'react'
import LoginProfile from '../insidelogin/topbar/LoginProfile';
import RightSide from './RightSide';
import { LeftSide } from './LeftSide';

export class MainLoginNews extends Component {
    render() {
        return (
            <React.Fragment>
                <div className="container-fluid main-padding">
                    <LoginProfile/>
                    <hr/>
                    <hr/>
                    <hr className="bg-dark"/>
                    <div class="row">
                        <LeftSide/>
                        <RightSide/>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default MainLoginNews;
