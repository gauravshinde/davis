import React from "react";

import { HeadingBox } from "../headingcomponent/HeadingBox";
import Articles from "../../json/notify/Articles.json";
import Reports from "../../components/reports/Reports";
import Gearicon from "../../assets/images/header/Vector.png";

const RightSide = () => {
  return (
    <React.Fragment>
      <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 Reports-main">
        <div className="sidebar-item">
          <div className="make-me-sticky">
            <HeadingBox
              firstdiv={"heading-black"}
              titleClassName={"titleClassName"}
              title={"Notices"}
              gearicon={Gearicon}
            />
            <div className="card border-0">
              <div className="card-body p-0">
                {Articles.map((article, i) => (
                  <Reports
                    key={i}
                    title={article.title}
                    paragraph={article.paragraph}
                  />
                ))}
              </div>
            </div>
            <br />
            <HeadingBox
              firstdiv={"heading-black"}
              titleClassName={"titleClassName"}
              title={"Relevant Reports"}
            />
            <div className="card border-0">
              <div className="card-body p-0">
                {Articles.map((article, i) => (
                  <Reports
                    key={i}
                    title={article.title}
                    paragraph={article.paragraph}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default RightSide;
