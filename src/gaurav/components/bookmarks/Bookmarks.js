import React from 'react';
import { Link } from 'react-router-dom';
import Bookmarksj from '../../json/bookmarks/Bookmarks';
import LoginProfile from '../insidelogin/topbar/LoginProfile';
import NavBar from '../dashboard/NavLink';

const Bookmarks = () => {
    return (
        <React.Fragment>
            <section className="main-padding">
                    <LoginProfile/>
                    <NavBar/>
                <div className="container-fluid association-main">
                    {Bookmarksj.map((bookmarks, index) => {
                        return  <div className="row" key={bookmarks._id}>
                                    <div className="col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                                        <h4>{bookmarks.bookmarks_title}</h4>
                                        <p className="bookmarks-para">{bookmarks.bookmarks_para}</p>
                                    </div>
                                    <div className="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right align-self-end">
                                        <span>{bookmarks.bookmarks_location}</span><br/>
                                        <span>{bookmarks.bookmarks_date} | {bookmarks.bookmarks_time} Local time</span>
                                    </div>
                                    <div className="col-12">
                                        <hr/>
                                    </div>
                                </div>
                    })}
                    <ul className="pagination">
                        <li className="page-item"><Link className="page-link" to="">1</Link></li>
                        <li className="page-item"><Link className="page-link" to="">2</Link></li>
                        <li className="page-item"><Link className="page-link" to="">3</Link></li>
                        <li className="page-item"><Link className="page-link" to="">4</Link></li>
                        <li className="page-item"><Link className="page-link" to="">5</Link></li>
                    </ul>
                </div>

            </section>
        </React.Fragment>
    )
}

export default Bookmarks;
