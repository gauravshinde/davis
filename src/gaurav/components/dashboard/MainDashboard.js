import React, { Component } from 'react';
import LoginProfile from '../insidelogin/topbar/LoginProfile';
import NavBar from './NavLink';
import Slider from "react-slick";
import Articles from '../../json/notify/Articles.json';
import Reports from '../reports/Reports';
import { HeadingBox } from '../headingcomponent/HeadingBox';
import imageone from '../../assets/images/mainheader/imageone.jpg';
import imagetwo from '../../assets/images/mainheader/imagetwo.jpg';
import imagethree from '../../assets/images/mainheader/imagethree.jpg';
import Recentarticles from '../../json/recentarticles/recent.json';
import Blogdashboard from '../insidelogin/blog/Blogdashboard';
import BlogNews from '../../json/blog/BlogNews.json';
import { LeftComponent } from "./../insidelogin/blog/BlogComponentone";


export class MainDashboard extends Component {
    render() {

        var settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
          };

        return (
            <React.Fragment>
              <div className="container-fluid main-padding">
                <LoginProfile/>
                <NavBar/>

                <div className="row mt-4">
                  <div className="col-md-8">
                    <Slider {...settings}>
                      <div>
                        <div
                          style={{
                              backgroundColor: "gray",
                              width: "100%",
                              height: "300px"
                            }}
                          >
                          <img src={imageone} alt="slideone" className="img-fluid"/>
                        </div>
                        <div
                          style={{
                              backgroundColor: "black",
                              width: "100%",
                              color: "white",
                              textAlign: "center"
                            }}
                          >
                          hello
                        </div>
                      </div>
                      <div>
                        <div
                          style={{
                              backgroundColor: "gray",
                              width: "100%",
                              height: "300px"
                            }}
                          >
                          <img src={imagetwo} alt="slideone" className="img-fluid"/>
                        </div>
                        <div
                          style={{
                            backgroundColor: "black",
                            width: "100%",
                            color: "white",
                            textAlign: "center"
                          }}
                        >
                          hello
                        </div>
                      </div>
                      <div>
                        <div
                          style={{
                              backgroundColor: "gray",
                              width: "100%",
                              height: "300px"
                            }}
                          >
                          <img src={imagethree} alt="slideone" className="img-fluid"/>
                        </div>
                        <div
                          style={{
                            backgroundColor: "black",
                            width: "100%",
                            color: "white",
                            textAlign: "center"
                          }}
                        >
                          hello
                        </div>
                      </div>
                    </Slider>
                  </div>
                  <div className="col-md-4 Reports-main">
                        <div className="sidebar-item">
                            <div className="make-me-sticky">
                                <HeadingBox 
                                    firstdiv={ 'heading-black' }
                                    titleClassName={ 'titleClassName' }
                                    title={ 'Relevant Notices'}
                                />
                                <div className="card border-0">
                                    <div className="card-body p-0">
                                    {
                                        Articles.map ( (article, i) => (
                                            <Reports
                                                key={i}
                                                title={article.title}
                                                paragraph={article.paragraph}
                                            />
                                        ))
                                    }
                                    </div>
                                </div>
                                <br/>
                                <HeadingBox 
                                    firstdiv={ 'heading-black' }
                                    titleClassName={ 'titleClassName' }
                                    title={ 'Relevant Reports'}
                                />
                                <div className="card border-0">
                                    <div className="card-body p-0">
                                    {
                                        Articles.map ( (article, i) => (
                                            <Reports
                                                key={i}
                                                title={article.title}
                                                paragraph={article.paragraph}
                                            />
                                        ))
                                    }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-8 mt-5">
                      <HeadingBox
                        firstdiv={"heading-black"}
                        titleClassName={"titleClassName my-1"}
                        title={"Blogs"}
                      />
                      {BlogNews.map((blog, i) => (
                        <LeftComponent
                          main={true}
                          key={i}
                          Blogimage={blog.image}
                          headline={blog.title}
                          summary={blog.summary}
                          date={blog.date}
                          time={blog.time}
                        />
                      ))}
                    </div>
                  </div>

                  <div className="row">
                    {/* Blog section */}
                    <div className="col-12">
                    <HeadingBox 
                      firstdiv={ 'heading-black' }
                      titleClassName={ 'titleClassName' }
                      title={ 'Recent Articles' } 
                    />
                    {
                            Recentarticles.map ( (recent, i) =>(
                                <Blogdashboard
                                key={i}
                                Blogimage={recent.image}
                                headline={recent.title}
                                summary={recent.Summary}
                                summarytwo={recent.Summaryone}
                                />
                            )) 
                        }

                    </div>
                  </div>
                </div> 
            </React.Fragment>
        )
    }
}

export default MainDashboard;
