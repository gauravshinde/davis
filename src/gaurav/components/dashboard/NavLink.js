import React, { Component } from 'react';
import NavItem from './ReusableComponents/NavItem';
import {withRouter} from 'react-router-dom'

export class NavLink extends Component {
    constructor(){
        super();
        this.state = {
            currentNavBar:''
        }
    }

    onClickNav = value => e =>{
        this.props.history.push(`/${value}`)
        console.log(value)
    }
    render() {
        return (
            <div id='dashboard' className='main-nav-container'>
                <NavItem name={"Sean"} classes={this.state.currentNavBar === "Sean" ? 'nav-yellow-active' : 'nav-yellow'} onClick={ this.onClickNav("maindashboard") } displayLine={true} lineClasses={'nav-line-yellow'} />
                <NavItem name={"Prices"} classes={this.state.currentNavBar === "Prices" ? 'nav-yellow-active' : 'nav-yellow'} onClick={ this.onClickNav("Prices") } displayLine={true} lineClasses={'nav-line-yellow'} />
                <NavItem name={"News"} classes={this.state.currentNavBar === "News" ? 'nav-yellow-active' : 'nav-yellow'} onClick={ this.onClickNav("News") } displayLine={true} lineClasses={'nav-line-yellow'} />
                <NavItem name={"Exchanges"} classes={this.state.currentNavBar === "Exchanges" ? 'nav-yellow-active' : 'nav-yellow'} onClick={ this.onClickNav("Exchanges") } displayLine={true} lineClasses={'nav-line-yellow'} />
                <NavItem name={"Reports"} classes={this.state.currentNavBar === "Reports" ? 'nav-yellow-active' : 'nav-yellow'} onClick={ this.onClickNav("Reports") } displayLine={true} lineClasses={'nav-line-yellow'} />
                <NavItem name={"Blogs"} classes={this.state.currentNavBar === "Blogs" ? 'nav-yellow-active' : 'nav-yellow'} onClick={ this.onClickNav("BlogNews") } displayLine={true} lineClasses={'nav-line-yellow'} />
                <NavItem name={"Resources"} classes={this.state.currentNavBar === "Resources" ? 'nav-yellow-active' : 'nav-yellow'} onClick={ this.onClickNav("Association") } displayLine={true} lineClasses={'nav-line-yellow'} />
                <NavItem name={"Bookmarks"} classes={this.state.currentNavBar === "Bookmarks" ? 'nav-yellow-active' : 'nav-yellow'} onClick={ this.onClickNav("Bookmarks") } displayLine={true} lineClasses={'nav-line-yellow'} />
            </div>
        )
    }
}

export default (withRouter(NavLink));
