import React, { Component, Fragment } from 'react';
import NavItem from './ReusableComponents/NavItem';

import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

// import UserProfile from './Profile/Profile';
// import Administrator from './Administrator/Administrator';
// import SubAdmin from './SubAdmins/SubAdmins';

export class Profile extends Component {
    constructor(){
        super();
        this.state = {
            profileNav:''
        }
    }

    componentDidMount(){
        this.setState({
            profileNav:'Profile'
        })
    }

    onClick = value => e => {
        this.setState({
            profileNav:value
        })
    }
    render() {
        return (
            <Fragment>
                <div id="dashboard" className='container-fluid'>
                    <div className='row'>
                        <div className='col-sm-12'>
                        <NavItem name={"Profile"} classes={this.state.profileNav === "Profile" ? 'nav-black-active' : 'nav-black'} onClick={ this.onClick("Profile") } displayLine={true} lineClasses={'nav-line-black'} />
                        {
                            this.props.auth.user.isSuperAdmin ? <Fragment>
                                <NavItem name={"Administrator"} classes={this.state.profileNav === "Administrator" ? 'nav-black-active' : 'nav-black'} onClick={ this.onClick("Administrator") } displayLine={true} lineClasses={'nav-line-black'} />
                                <NavItem name={"Sub Admins"} classes={this.state.profileNav === "Sub Admins" ? 'nav-black-active' : 'nav-black'} onClick={ this.onClick("Sub Admins") } displayLine={true} lineClasses={'nav-line-black'} />
                                </Fragment> : null
                        }
                        </div>
                        <div className='col-sm-12'><hr className='hr-line'/></div>
                        <div className='col-sm-12'>
                            <div className='margin-top-line'></div>
                            <hr className='hr-line' />
                        </div>
                    </div>
                </div>
                {/* {
                    this.state.profileNav === 'Profile' ? <UserProfile/> : null
                }
                {
                    this.state.profileNav === 'Administrator' ? <Administrator /> : null
                }
                {
                    this.state.profileNav === 'Sub Admins' ? <SubAdmin/> : null
                } */}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    auth : state.auth
});

export default connect(mapStateToProps, {  })(withRouter(Profile));