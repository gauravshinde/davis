import React, { Fragment }  from 'react';

const NavItem = ({
    name,
    classes,
    onClick,
    lineClasses,
    stateName,
    displayLine
}) => {
    return (
        <Fragment>
        <div name={stateName} onClick={onClick} className={classes}>{ name }</div>
        { displayLine ? <div className={lineClasses}></div> : null }
    </Fragment>
    )
}

export default NavItem;