import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Footer extends Component {
  render() {
    return (
      <React.Fragment>
        <section className="main-padding">
            <div className="container-fluid footer-main">
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <ul type="none">
                            <li><Link to="">Notices</Link></li>
                            <li><Link to="">Methodology</Link></li>
                            <li><Link to="">Reports</Link></li>
                            <li><Link to="">Blog</Link></li>
                            <li><Link to="">Prices</Link></li>
                            <li><Link to="">Association news</Link></li>
                            <li><Link to="">Events</Link></li>
                        </ul>
                    </div>
                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <ul type="none">
                            <li><Link to="">Contact Us</Link></li>
                            <li><Link to="">Support</Link></li>
                            <li><Link to="">Cookies</Link></li>
                            <li><Link to="">Copyright Information</Link></li>
                            <li><Link to="">Terms and Conditions</Link></li>
                            <li><Link to="">Disclaimer</Link></li>
                            <li><Link to="">Data Solutions</Link></li>
                            <li><Link to="">Data Licensing</Link></li>
                        </ul>
                    </div>
                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4"></div>
                </div>
            </div>
        </section> 
      </React.Fragment>
    )
  }
}

export default Footer;
