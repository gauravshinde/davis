import React from 'react'
import Gearicon from '../../assets/images/header/Vector.png';
import { HeadingBox } from './HeadingBox';

const AllHeadingBox = () => {
    return (
        <React.Fragment>
            <div className="container-fluid main-padding">
                <HeadingBox 
                firstdiv={ 'heading-black' }
                titleClassName={ 'titleClassName' }
                title={ 'Soul Index News'}
                gearicon={Gearicon}
                geariconclass={ 'geariconclass' }
            />
            </div>
        </React.Fragment>
    )
}

export default AllHeadingBox;
