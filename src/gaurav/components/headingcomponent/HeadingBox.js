import React from 'react';
import { Link } from 'react-router-dom';

export const HeadingBox = ({
    firstdiv,
    seconddiv,
    title,
    titleClassName,
    gearicon,
    geariconclass,
    paragraphone,
    paragraphtwo,
    paragraphclassone,
    paragraphclasstwo,
    boldtext,
    boldclass    
}) => {
    return (
        <React.Fragment>
            <div className={firstdiv} style={{ marginTop: "5px", marginBottom: "5px" }}>
                <h3 className={ titleClassName }>{title}</h3>
                <Link to="" className="gear-link" data-target="#gear" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src={gearicon} className={ geariconclass } alt=""/></Link>
                <div id="#gear" className="dropdown-menu dropdown-menu-right gear-custommain">
                    <button className="dropdown-item p-0" type="button">Move to top of watchlist</button>
                    <div className="dropdown-divider gear-divider-custom"></div>
                    <button className="dropdown-item p-0" type="button">Remove from watchlist</button>
                    <div className="dropdown-divider gear-divider-custom"></div>
                    <button className="dropdown-item p-0" type="button">Set Email Alert</button>
                    <div className="dropdown-divider gear-divider-custom"></div>
                </div>
            </div>
            <div className={ seconddiv }>
                <p className={ paragraphclassone }>{paragraphone}</p>
                <p className={ paragraphclasstwo }>{paragraphtwo}<Link to={''} className={ boldclass }>{boldtext}</Link></p>
            </div>
        </React.Fragment>
    )
}

