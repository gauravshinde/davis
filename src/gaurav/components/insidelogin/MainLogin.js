import React from 'react';
import LoginProfile from './topbar/LoginProfile';
import Topmenu from './topbar/Topmenu';

const MainLogin = () => {
    return (
        <React.Fragment>
            <div className="container-fluid main-padding">
                <LoginProfile/>
                <Topmenu/>
            </div>
        </React.Fragment>
    )
}

export default MainLogin;
