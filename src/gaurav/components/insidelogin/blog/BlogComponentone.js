import React from "react";

export const LeftComponent = ({
  Blogimage,
  headline,
  summary,
  date,
  time,
  main
}) => {
  if (main && summary !== "" && Blogimage !== "") {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-sm-4">
            {Blogimage !== "" ? (
              <img src={Blogimage} className="img-fluid" alt="" />
            ) : null}
          </div>
          <div className="col-sm-8">
            <h4>{headline}</h4>
            <p>{summary}</p>
            <div className="col-12 p-0 text-right align-self-end">
              {/* <span>Location</span><br/> */}
              <span>
                {date} | {time}
              </span>
            </div>
            <hr className="mt-0 border-dark" />
          </div>
        </div>
      </div>
    );
  }
  return (
    <React.Fragment>
      <div className="bolog-news-left">
        {Blogimage !== "" ? (
          <img src={Blogimage} className="img-fluid" alt="" />
        ) : null}
        <h4>{headline}</h4>
        <p>{summary}</p>
        <div className="col-12 p-0 text-right align-self-end">
          {/* <span>Location</span><br/> */}
          <span>
            {date} | {time}
          </span>
        </div>
        <hr className="mt-0 border-dark" />
      </div>
    </React.Fragment>
  );
};