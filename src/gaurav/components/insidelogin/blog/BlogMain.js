import React from 'react';
import { LeftComponent } from './BlogComponentone';
import BlogNews from '../../../json/blog/BlogNews.json';
import { HeadingBox } from '../../headingcomponent/HeadingBox';
import Reports from '../../reports/Reports';
import Articles from '../../../json/notify/Articles.json';
import LoginProfile from '../../insidelogin/topbar/LoginProfile';
import NavBar from '../../dashboard/NavLink';

const BlogMain = () => {
    return (
        <React.Fragment>
            
            <div className="container-fluid main-padding">
                <div className="col-12 p-0 mb-4">
                    <LoginProfile/>
                    <NavBar/>
                </div>
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                        {
                            BlogNews.map ( (blog, i) =>(
                                <LeftComponent 
                                key={i}
                                Blogimage={blog.image}
                                headline={blog.title}
                                summary={blog.summary}
                                date={blog.date}
                                time={blog.time}
                                />
                            )) 
                        }
                    </div>
                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 Reports-main">
                        <div className="sidebar-item">
                            <div className="make-me-sticky">
                                <HeadingBox 
                                    firstdiv={ 'heading-black' }
                                    titleClassName={ 'titleClassName' }
                                    title={ 'Relevant Notices'}
                                />
                                <div className="card border-0">
                                    <div className="card-body p-0">
                                    {
                                        Articles.map ( (article, i) => (
                                            <Reports
                                                key={i}
                                                title={article.title}
                                                paragraph={article.paragraph}
                                            />
                                        ))
                                    }
                                    </div>
                                </div>
                                <br/>
                                <HeadingBox 
                                    firstdiv={ 'heading-black' }
                                    titleClassName={ 'titleClassName' }
                                    title={ 'Relevant Reports'}
                                />
                                <div className="card border-0">
                                    <div className="card-body p-0">
                                    {
                                        Articles.map ( (article, i) => (
                                            <Reports
                                                key={i}
                                                title={article.title}
                                                paragraph={article.paragraph}
                                            />
                                        ))
                                    }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default BlogMain;
