import React from 'react';

export const Blogdashboard = ({
    Blogimage,
    headline,
    summary,
    summarytwo,
}) => {
    return (
        <React.Fragment>
            <div className="row mt-3">
            <div className="col-md-5">
                { Blogimage !== "" ? <img src={Blogimage} className="img-fluid" alt=""/> : null }
            </div>
            <div className="col-md-7">
                <h4 className="mt-2">{headline}</h4>
                <p className="mt-3">{summary}</p>
                <p className="mt-3">{summarytwo}</p>
            </div>
            </div>
        </React.Fragment>
    )
}

export default Blogdashboard;


