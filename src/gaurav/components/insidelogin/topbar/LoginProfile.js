import React from 'react';
import { Link } from 'react-router-dom';
import Profile from '../../../assets/images/header/Logo.png';

const LoginProfile = () => {
    return (
        <React.Fragment>
            <div className="container-fluid login-profile-main">
                <div className="row">
                    <div className="col-12 p-0 d-flex justify-content-end">
                        <img src={Profile} className="img-fluid" alt="User Profile" />
                        <Link to="" className="dropdown-toggle login-profile-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span>Curtis James<br/>Jackson</span>
                        </Link>
                        <div className="dropdown-menu dropdown-menu-right dropdown-custommain">
                            <button className="dropdown-item p-0" type="button">Profile</button>
                            <div className="dropdown-divider dropdown-divider-custom"></div>
                            <button className="dropdown-item p-0" type="button">Administrator</button>
                            <div className="dropdown-divider dropdown-divider-custom"></div>
                            <button className="dropdown-item p-0" type="button">Editorial</button>
                            <div className="dropdown-divider dropdown-divider-custom"></div>
                            <button className="dropdown-item p-0" type="button">Soul Index</button>
                            <div className="dropdown-divider dropdown-divider-custom"></div>
                            <button className="dropdown-item p-0" type="button"></button>
                            <div className="dropdown-divider dropdown-divider-custom"></div>
                            <button className="dropdown-item text-right" type="button">Logout</button>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default LoginProfile;
