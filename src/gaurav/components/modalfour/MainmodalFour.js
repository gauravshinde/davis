import React from 'react'
import SigninPurchesed from './SigninPurchesed';
import ProfileUserfree from './ProfileUserfree';

const Mainmodal = () => {
    return (
        <React.Fragment>
            <div class="container-fluid main-padding">
                <div class="row">
                    <SigninPurchesed/>
                    <ProfileUserfree/>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Mainmodal
