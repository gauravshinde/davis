import React, { Component } from 'react'

export class ProfileUserfree extends Component {
    render() {
        return (
            <React.Fragment>
                <section>
                    {/* <!-- Button trigger modal --> */}
                    <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal31">
                    Single Purchesed
                    </button>

                    {/* <!-- Modal --> */}
                    <div className="modal fade bd-example-modal-sm" id="exampleModal31" role="dialog" aria-labelledby="exampleModalLabel31" aria-hidden="true">{/*tabindex="-1"*/}
                        <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="col-12 modal-close-button">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="col-12">
                                    <h4>This Report on That Thing</h4>
                                    <span>200$</span>
                                    <p>Payment Successful!</p>
                                    <hr/>
                                    <p className="paragraph-css">Thank you for buying the report. You can go read it now.</p>
                                </div>
                                <div className="col-12 text-center">
                                    <button type="button" className="btn button-css mb-4">Done</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}

export default ProfileUserfree;
