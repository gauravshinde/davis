import React, { Component } from 'react'

export class SigninPurchesed extends Component {
    render() {
        return (
            <React.Fragment>
                <section>
                    {/* <!-- Button trigger modal --> */}
                    <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal30">
                    Single Purchesed
                    </button>

                    {/* <!-- Modal --> */}
                    <div className="modal fade bd-example-modal-sm" id="exampleModal30" role="dialog" aria-labelledby="exampleModalLabel30" aria-hidden="true">{/*tabindex="-1"*/}
                        <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                            <div className="modal-content">
                                <div className="col-12 modal-close-button">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="col-12">
                                    <h4>This Report on That Thing</h4>
                                    <span>200$</span>
                                </div>
                                <div className="col-12 d-flex justify-content-between">
                                    <button type="button" className="btn cancel-css">Cancel</button>
                                    <button type="button" className="btn buy-css">Buy Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        )
    }
}

export default SigninPurchesed;
