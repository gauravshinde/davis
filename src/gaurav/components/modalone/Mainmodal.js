import React from 'react';
import Modalone from '../modalone/Modalone';
import ModalTwo from '../modalone/ModalTwo';
import ModalThree from '../modalone/ModalThree';
import ModalFour from '../modalone/ModalFour';
import ModalFive from '../modalone/ModalFive';
import ModalSix from '../modalone/ModalSix';
import ModalSeven from '../modalone/ModalSeven';
import ModalEight from '../modalone/ModalEight';
import ModalNine from '../modalone/ModalNine';

const Mainmodal = () => {
    return (
        <React.Fragment>
            <section className="mt-5">
                <div className="container">
                    <Modalone/>
                    <ModalTwo/>
                    <ModalThree/>
                    <ModalFour/>
                    <ModalFive/>
                    <ModalSix/>
                    <ModalSeven/>
                    <ModalEight/>
                    <ModalNine/>
                </div>
            </section>
        </React.Fragment>
    )
}

export default Mainmodal;
