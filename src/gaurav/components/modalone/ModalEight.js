import React from 'react';

const ModalEight = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal8">
                05PERSON PROFILE ADMIN-SUccess Two
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal8" role="dialog" aria-labelledby="exampleModalLabel8" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12 text-center">
                                <p>Welcome to Davis Index</p>
                                <hr className="mt-0 mb-0"/>
                                <button type="button" className="btn add-organisation-button">Add your organisation <i className="fa fa-arrow-right arrow-icon" aria-hidden="true"></i></button>
                                <p className="modaleight-paragraph">Or Continue as single user</p>
                            </div>
                            <div className="col-12 text-center">
                                <button type="button" className="btn button-css modaleight-next">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default ModalEight;
