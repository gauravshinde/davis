import React from 'react';

const ModalFive = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal5">
                04PERSON PROFILE PAYMENT PROCESS
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal5" role="dialog" aria-labelledby="exampleModalLabel5" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>PAYMENT GATEWAY</p>
                                <hr className="mt-0 payment-getway-border"/>
                            </div>
                            <div className="col-8 m-auto">
                                <p className="modalfive-paragraph">Things stripe does happen here.</p>
                            </div>
                            <div className="col-12 text-center">
                                <button type="button" className="btn button-css modalfive-next">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default ModalFive;
