import React from 'react';

const ModalFour = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal4">
                03PERSON PROFILE SHOPPING CART
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal4" role="dialog" aria-labelledby="exampleModalLabel4" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12 text-center">
                                <p>Welcome to Soul</p>
                            </div>
                            <div className="col-9 m-auto">
                                <p className="modalfour-paragraph">You have signed up for the Soul US Materials and Auto recycling news, prices and analysis.</p>
                            </div>
                            <div className="col-12">
                                <form>
                                    <table className="table mb-0">
                                        <tbody>
                                            <tr>
                                                <td className="text-left">Materials USA $365</td>
                                                <td className="text-right">$365.00</td>
                                            </tr>
                                            <tr>
                                                <td className="text-left">Auto Recycling</td>
                                                <td className="text-right">$120.00</td>
                                            </tr>                                    
                                        </tbody>
                                    </table>
                                    <div className="col-12 border-top">
                                        <p className="total-ptice"><span>$485.00</span></p>
                                    </div>
                                </form>
                            </div>
                            <div className="col-12 text-center">
                                <button type="button" className="btn button-css modalfour-next">Pay Now</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default ModalFour;
