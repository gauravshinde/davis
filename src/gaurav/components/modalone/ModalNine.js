import React from 'react';

const ModalNine = () => {
    return (
        <React.Fragment>
            <section>
            {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal9">
                02PERSON PROFILE REJECT
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-lg" id="exampleModal9" role="dialog" aria-labelledby="exampleModalLabel9" aria-hidden="true">{/*tabindex="-1"*/}
                <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="col-12 modaltwo-close-button">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="col-9 m-auto">
                            <p>Terms & Conditions</p>
                        </div>
                        <div className="col-12">
                            <hr/>
                        </div>
                        <div className="col-8 m-auto">
                            <p className="modalnine-paragraph">You seem to be unhappy with the terms laid out for use of this service. Please let us know why and we can make our legal team aware of your grievences and if we can address them. We shall get in touch with you over email once we have had a chance to review your message.</p>
                        </div>
                        <div className="col-12">
                            <hr/>
                        </div>
                        <div className="col-8 m-auto modalnine-height">
                            <p className="modalnine-paragraph">Write your objections here.</p>
                        </div>
                        <div className="col-11 m-auto">
                            <button type="button" className="btn button-css modalbottom-next pull-left" data-dismiss="modal">Back</button>
                            <button type="button" className="btn button-css modalbottom-next pull-right">Send</button>
                        </div>
                    </div>
                </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default ModalNine;
