import React from 'react';

const ModalSeven = () => {
    return (
        <React.Fragment>
            <section>

                {/* button */}

                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal7">
                05PERSON PROFILE ADMIN-SUccess
                </button>

                <div className="modal fade bd-example-modal-sm" id="exampleModal7" role="dialog" aria-labelledby="exampleModalLabel7" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>Sign In</p>
                                <form>
                                    <div className="row">
                                        <div className="col-12">
                                            <input type="email" className="form-control signup-input" name="email" id="email" placeholder="Enter Email Address" />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <input type="password" className="form-control signup-input" name="pass" id="pass" placeholder="***********" />
                                        </div>
                                    </div>
                                    <div className="col-12 text-center">
                                        <button type="button" className="btn button-css modalseven-next">Next</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default ModalSeven;
