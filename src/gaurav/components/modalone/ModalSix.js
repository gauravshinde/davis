import React from 'react';

const ModalSix = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal6">
                04PERSON PROFILE TEAM ADMIN
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal6" role="dialog" aria-labelledby="exampleModalLabel6" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>And you are done!</p>
                                <hr className="mt-0"/>
                            </div>
                            <div className="col-8 m-auto">
                                <p className="modalsix-paragraph">You will be sent an email confirming your account post payment. Click on verification link to start using Davis Index.</p>
                            </div>
                            <div className="col-12 text-center">
                                <button type="button" className="btn button-css modalsix-next">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default ModalSix;
