import React from 'react';

const ModalThree = () => {
    return (
        <React.Fragment>
            <section>
            {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal3">
                02PERSON PROFILE TERMS
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-lg" id="exampleModal3" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">{/*tabindex="-1"*/}
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="col-12 modaltwo-close-button">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="col-9 m-auto">
                            <p>Terms & Conditions</p>
                        </div>
                        <div className="col-12">
                            <hr/>
                        </div>
                        <div className="col-8 m-auto">
                            <p className="modalthree-paragraph">Lorem ipsum euismod tellus curae ut, odio litora lorem turpis malesuada taciti, nulla mollis eget velit egestas mollis eget diam ligula libero facilisis aenean amet.</p>
                            <p className="modalthree-paragraph">Habitasse primis rutrum mattis tempus pretium duis rhoncus, auctor volutpat mollis tristique mauris curabitur ornare consequat, curabitur in lacus orci morbi justo.</p>
                            <p className="modalthree-paragraph">Phasellus cras condimentum mauris nisi torquent nostra fringilla, volutpat metus odio eget class posuere, urna duis cras lectus porttitor pulvinar elit vulputate adipiscing nec ac aliquam dictumst mi taciti non.</p>
                            <p className="modalthree-paragraph">Curabitur convallis mauris metus aenean donec sollicitudin, mi facilisis pulvinar torquent phasellus, dolor placerat elementum placerat sed sagittis auctor est etiam erat dictumst porttitor, euismod nisl phasellus lectus sem integer iaculis, aenean sollicitudin purus quis suspendisse faucibus volutpat ultricies convallis fermentum quisque, urna vitae consequat aenean potenti, sit elit erat mattis.</p>
                            <p className="modalthree-paragraph">Interdum risus inceptos fusce feugiat commodo ante, magna venenatis habitant etiam erat metus platea, maecenas senectus porta eros cras etiam nulla auctor nunc eu mattis suscipit, vehicula sociosqu hac duis non nisl, netus pulvinar ante dictumst rhoncus.</p>
                        </div>
                        <div className="col-11 m-auto signup-marginthree">
                            <button type="button" className="btn button-css modalbottom-next pull-left" data-dismiss="modal">Reject</button>
                            <button type="button" className="btn button-css modalbottom-next pull-right">Accept</button>
                        </div>
                    </div>
                </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default ModalThree;
