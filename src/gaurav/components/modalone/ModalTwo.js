import React from 'react';

const ModalTwo = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal2">
                01PERSON PROFILE ADMIN PRODUCT
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal2" role="dialog" aria-labelledby="exampleModalLabel2" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>Sign Up</p>
                            </div>
                            <div className="col-12">
                                <ul className="nav nav-pills border-top" id="pills-tab" role="tablist">
                                    <li className="nav-item">
                                        <a className="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Guest Account</a>
                                    </li>
                                    <li className="nav-item m-auto border-left">
                                        <a className="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Member</a>
                                    </li>
                                </ul>
                                <div className="tab-content" id="pills-tabContent">
                                    <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <form>
                                            <table className="table mb-0">
                                                <tbody>
                                                    <tr>
                                                    <td>Materials USA</td>
                                                    <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                    </tr>
                                                    <tr>
                                                    <td>Materials INDIA</td>
                                                    <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                    </tr>
                                                    <tr>
                                                    <td>Automobile</td>
                                                    <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                    </tr>
                                                    <tr>
                                                    <td>Engineering</td>
                                                    <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div className="col-12 border-top modal-two-height">
                                                <p className="modaltwo-price-total pull-left">TOTAL</p>
                                                <p className="modaltwo-price-totalone pull-right">$365</p>
                                            </div>
                                            <div className="col-12 text-center">
                                                <button type="button" className="btn button-css modal-button-next">Sign Up</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        <form>
                                            <table className="table mb-0">
                                                <tbody>
                                                    <tr>
                                                    <td>Materials USA &nbsp;&nbsp;&nbsp;&nbsp;$0</td>
                                                    <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                    </tr>
                                                    <tr>
                                                    <td>Materials INDIA &nbsp;&nbsp;&nbsp;&nbsp;$0</td>
                                                    <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                    </tr>
                                                    <tr>
                                                    <td>Automobile &nbsp;&nbsp;&nbsp;&nbsp;$0</td>
                                                    <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                    </tr>
                                                    <tr>
                                                    <td>Engineering &nbsp;&nbsp;&nbsp;&nbsp;$0</td>
                                                    <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div className="col-12 border-top modal-two-height">
                                                <p className="modaltwo-price-total pull-left">TOTAL</p>
                                                <p className="modaltwo-price-totalone pull-right">$365</p>
                                            </div>
                                            <div className="col-12 text-center">
                                                <button type="button" className="btn button-css modal-button-next">Sign Up</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default ModalTwo;
