import React, { Component } from 'react';

class Modalone extends Component {
    render() {
        return (
            <React.Fragment>
            <section>

                {/* button */}

                <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                00PERSON PROFILE ADMIN SIGN UP
                </button>

                <div className="modal fade bd-example-modal-sm" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>Sign Up</p>
                                <form>
                                    <div className="row">
                                        <div className="col-12">
                                            <input type="text" className="form-control signup-input" name="name" id="name" placeholder="Enter Name" />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <input type="email" className="form-control signup-input" name="email" id="email" placeholder="Enter Email Address" />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <input type="password" className="form-control signup-input" name="pass" id="pass" placeholder="***********" />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12">
                                            <input type="password" className="form-control signup-input signup-margin" name="confirmpass" id="confirmpass" placeholder="***********" />
                                        </div>
                                    </div>
                                    <div className="col-12 text-center">
                                        <button type="button" className="btn button-css modal-button-next">Next</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
        )
    }
}

export default Modalone;

