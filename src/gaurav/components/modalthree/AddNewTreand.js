import React from 'react'

const AddNewTreand = () => {
    return (
        <React.Fragment>
            {/* <!-- Button trigger modal --> */}
            <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal29">
            01 TRENDWATCH
            </button>

            {/* <!-- Modal --> */}
            <div className="modal fade" id="exampleModal29" role="dialog" aria-labelledby="exampleModal29" aria-hidden="true">{/*tabindex="-1"*/}
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content modal-borderlisttwo">
                        <div className="col-12 modal-close-button">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form>
                            <div className="col-12 news-list-padding">
                                <p>Add New TrendWatch</p>
                                <div className="form-group add-inputmargin">
                                    <input type="text" className="form-control newslist-input-border" name="" id="" placeholder="Enter name of the Trend here."/>
                                </div>
                            </div>
                            <div className="col-12 text-center">
                                <button type="submit" className="btn button-css modal-button-next mb-5">Done</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default AddNewTreand;
