import React from 'react'

const AddNewWishlist = () => {
    return (
        <React.Fragment>
            {/* <!-- Button trigger modal --> */}
            <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal28">
            01WATCHLIST-ALL SIX
            </button>

            {/* <!-- Modal --> */}
            <div className="modal fade" id="exampleModal28" role="dialog" aria-labelledby="exampleModal28" aria-hidden="true">{/*tabindex="-1"*/}
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content modal-borderlisttwo">
                        <div className="col-12 modal-close-button">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form>
                            <div className="col-12 news-list-padding">
                                <p>Add New Watchlist</p>
                                <div className="form-group add-inputmargin">
                                    <input type="text" className="form-control newslist-input-border" name="" id="" placeholder="Enter name of the Watchlist here."/>
                                </div>
                            </div>
                            <div className="col-12 text-center">
                                <button type="submit" className="btn button-css modal-button-next mb-5">Done</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default AddNewWishlist;
