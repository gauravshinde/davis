import React from 'react';
import NewsList from './NewsList';
import PriceWatch from './PriceWatch';
import AddNewWishlist from './AddNewWishlist';
import AddNewTreand from './AddNewTreand';

const MainModalThree = () => {
    return (
        <section className="mt-5">
            <div className="container">
                <NewsList/>
                <PriceWatch/>
                <AddNewWishlist/>
                <AddNewTreand/>
            </div>
        </section>
    )
}

export default MainModalThree;
