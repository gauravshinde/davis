import React from 'react'

const NewsList = () => {
    return (
        <React.Fragment>
            {/* <!-- Button trigger modal --> */}
            <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal26">
            01NEWSLIST
            </button>

            {/* <!-- Modal --> */}
            <div className="modal fade" id="exampleModal26" role="dialog" aria-labelledby="exampleModal26" aria-hidden="true">{/*tabindex="-1"*/}
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content modal-borderlisttwo">
                        <div className="col-12 modal-close-button">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form>
                            <div className="col-12 news-list-padding">
                                <p>Set Newslist</p>
                                <div className="form-group">
                                    <input type="text" className="form-control newslist-input-border" name="" id="" placeholder="Type in to select catagory for Newswatch"/>
                                    <input type="text" className="form-control newslist-input-border" name="" id="" placeholder="Enter name of the Newswatch here."/>
                                    <select className="form-control newslist-input-border select-box-margin" id="exampleFormControlSelect1">
                                        <option>Select which Watchlist to add this to</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-12 text-center">
                                <button type="submit" className="btn button-css modal-button-next mb-5">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default NewsList;
