import React from 'react'

const AddTeammate = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal23">
                01PERSON PROFILE ADMIN PRODUCT
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal23" role="dialog" aria-labelledby="exampleModalLabel23" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content">
                            <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>Add Teammate</p>
                            </div>
                            <div className="col-12">
                                <form>
                                    <div className="form-group mb-0">
                                        <input type="text" name="" id="" className="form-control teammats-input" placeholder="Enter Name" required/>
                                        <input type="email" name="" id="" className="form-control teammats-input" placeholder="Enter Email Address" required/>
                                    </div>
                                    <div className="form-check check-box">
                                        <label className="form-check-label col-11 check-box1">Make Admin</label>
                                        <input type="checkbox" className="form-check-input col-1 check-box1" name="" id="" />
                                    </div>
                                    <div className="row">
                                    <span className="teammate-spam">Select from the list of publications the ones you want guesst and member access to.</span>  
                                    </div>

                                    <ul className="nav nav-pills border-top" id="pills-tab" role="tablist">
                                        <li className="nav-item">
                                            <a className="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Guest Account</a>
                                        </li>
                                        <li className="nav-item m-auto border-left">
                                            <a className="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Member</a>
                                        </li>
                                    </ul>
                                    <div className="tab-content" id="pills-tabContent">
                                        <div className="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                                <table className="table mb-0">
                                                    <tbody>
                                                        <tr>
                                                        <td>Materials USA $0</td>
                                                        <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                        </tr>
                                                        <tr>
                                                        <td>Materials INDIA $0</td>
                                                        <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                        </tr>
                                                        <tr>
                                                        <td>Automobile $0</td>
                                                        <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div className="col-12 border-top modal-two-height">
                                                    <p className="modaltwo-price-total pull-left">TOTAL</p>
                                                    <p className="modaltwo-price-totalone pull-right">$410</p>
                                                </div>
                                                <div className="col-12 p-0 mb-3 border-top border-bottom">
                                                    <button type="button" className="btn teammate-add-another">+ ADD ANOTHER TEAMMATES</button>
                                                </div>
                                                <div className="col-12 text-center">
                                                    <button type="button" className="btn button-css modal-button-next">Sign Up</button>
                                                </div>
                                        </div>
                                        <div className="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                                <table className="table mb-0">
                                                    <tbody>
                                                        <tr>
                                                        <td>Materials USA &nbsp;&nbsp;&nbsp;&nbsp;$0</td>
                                                        <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                        </tr>
                                                        <tr>
                                                        <td>Materials INDIA &nbsp;&nbsp;&nbsp;&nbsp;$0</td>
                                                        <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                        </tr>
                                                        <tr>
                                                        <td>Automobile &nbsp;&nbsp;&nbsp;&nbsp;$0</td>
                                                        <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                        </tr>
                                                        <tr>
                                                        <td>Engineering &nbsp;&nbsp;&nbsp;&nbsp;$0</td>
                                                        <td><input type="checkbox" className="form-check-input" name="" id="" value="checkedValue"/></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div className="col-12 border-top modal-two-height">
                                                    <p className="modaltwo-price-total pull-left">TOTAL</p>
                                                    <p className="modaltwo-price-totalone pull-right">$365</p>
                                                </div>
                                                <div className="col-12 text-center">
                                                    <button type="button" className="btn button-css modal-button-next">Pay</button>
                                                </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default AddTeammate;

