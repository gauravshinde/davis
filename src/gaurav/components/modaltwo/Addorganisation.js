import React from 'react'

const Addorganisation = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal13">
                Add organisation
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal13" role="dialog" aria-labelledby="exampleModalLabel13" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content ">
                            <div className="col-12 modal-close-button">
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="col-12">
                                    <p>Add organisation</p>
                                    <hr className="hr-border-clss"/>
                                    <form>
                                        <div className="form-group organisation-margin">
                                        <input type="text" className="form-control addorganisation-input-border" name="" id="" aria-describedby="helpId" placeholder="Enter Organisation Name"/>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-12 text-center">
                                    <button type="button" className="btn button-css modal-button-next">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        </React.Fragment>
    )
}

export default Addorganisation;
