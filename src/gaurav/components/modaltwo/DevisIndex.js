import React from 'react'

const DevisIndex = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal15">
                08PERSON PROFILE TEAM ADMIN
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal15" role="dialog" aria-labelledby="exampleModalLabel15" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content ">
                        <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12 text-center">
                                <p>Welcome to Davis Index</p>
                            </div>
                            <div className="col-9 m-auto ">
                                <p className="modalTworesend-paragraph">You have signed up for the DAVIS US Materials and Auto recycling news, prices and analysis.</p>
                            </div>
                            <div className="col-12">
                                <form>
                                    <table className="table mb-0">
                                        <tbody>
                                            <tr>
                                                <td className="text-left">Materials USA</td>
                                                <td className="toal-product"> x 2</td>
                                                <td className="text-right">$1095.00</td>
                                            </tr>
                                            <tr>
                                                <td className="text-left">Auto Recycling</td>
                                                <td className="toal-product"> X 3</td>
                                                <td className="text-right">$360.00</td>
                                            </tr> 
                                            <tr>
                                                <td className="text-left">Materials India</td>
                                                <td className="toal-product"> x 3</td>
                                                <td className="text-right">$720.00</td>
                                            </tr>                                    
                                        </tbody>
                                    </table>
                                    <div className="col-12 border-top">
                                        <p className="total-ptice"><span>$2,175.00</span></p>
                                    </div>
                                </form>
                            </div>
                            <div className="col-12 text-center mt-3">
                                <button type="button" className="btn button-css modal-button-next">Pay Now</button>
                            </div>
                        </div>
                    </div>
                    </div>
            </section>
        </React.Fragment>
    )
}

export default DevisIndex;
