import React from 'react'

const EditSubscription = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal22">
                11PERSON PROFILE USER-Free
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal22" role="dialog" aria-labelledby="exampleModalLabel22" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content ">
                        <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>Edit Subscription</p>
                                <span className="edit-subscription-title">First Name Last Name</span>
                                <hr/>
                                <p className="edit-subscription-paragraph">Are you sure you want to delete this account?</p>
                            </div>
                            <div className="col-12 text-center">
                                <button type="button" className="btn button-css modal-button-next">Delete</button>
                            </div>
                        </div>
                    </div>
                    </div>
            </section>
        </React.Fragment>
    )
}

export default EditSubscription;
