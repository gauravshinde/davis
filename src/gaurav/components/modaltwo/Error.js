import React from 'react'

const Error = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal14">
                07PERSON PROFILE TEAM ADMIN-RESEND INVITE Error
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal14" role="dialog" aria-labelledby="exampleModalLabel14" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content ">
                        <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>ERROR</p>
                                <span className="error-span-text">The invite to First name<br/>Failed. Please resend it<br/>manualy.</span>
                                <form>
                                    <div className="form-group error-margin">
                                    <input type="text" className="form-control error-input-border" name="" id="" aria-describedby="helpId" placeholder="you@awesome.com"/>
                                    </div>
                                </form>
                            </div>
                            <div className="col-12 text-center">
                                <button type="button" className="btn button-css modal-button-next">Send</button>
                            </div>
                        </div>
                    </div>
                    </div>
            </section>
        </React.Fragment>
    )
}

export default Error;
