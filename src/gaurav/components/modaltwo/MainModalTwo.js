import React from 'react';
import MainmodaltwoOne from '../modaltwo/MainmodaltwoOne';
import MainmodalResend from '../modaltwo/MainmodalResend';
import MainmodalCancel from '../modaltwo/MainmodalCancel';
import Addorganisation from '../modaltwo/Addorganisation';
import Error from './Error';
import DevisIndex from './DevisIndex';
import Tac from './Tac';
import TacReject from './TacReject';
import TacRejectTwo from './TacRejectTwo';
import TeamUserPassword from './TeamUserPassword';
import TeamUserpasstwo from './TeamUserpasstwo';
import WelcomLogin from './WelcomLogin';
import EditSubscription from './EditSubscription';
import AddTeammate from './AddTeammate';
import Editsubscriptionone from './Editsubscriptionone';
import Editsubscriptiontwo from './Editsubscriptiontwo';

const MainModalTwo = () => {
    return (
        <section className="mt-5">
            <div className="container">
                <MainmodaltwoOne/>
                <MainmodalResend/>
                <MainmodalCancel/>
                <Addorganisation/>
                <Error/>
                <DevisIndex/>
                <Tac/>
                <TacReject/>
                <TacRejectTwo/>
                <TeamUserPassword/>
                <TeamUserpasstwo/>
                <WelcomLogin/>
                <EditSubscription/>
                <AddTeammate/>
                <Editsubscriptionone/>
                <Editsubscriptiontwo/>
            </div>
        </section>
    )
}

export default MainModalTwo;
