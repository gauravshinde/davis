import React from 'react'

const MainmodalCancel = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal12">
                07PERSON PROFILE TEAM CANCEL INVITE
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal12" role="dialog" aria-labelledby="exampleModalLabel12" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content ">
                        <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12 text-center">
                                <p>Invitation Cancelled</p>
                            </div>
                            <div className="col-9 m-auto ">
                                <p className="modalTworesend-paragraph modalTworesend-margin">You have cancelled an invite sent to Jon B. His<br/>subscription shall be cancelled.</p>
                            </div>
                            <div className="col-12 text-center">
                                <button type="button" className="btn button-css modal-button-next">Done</button>
                            </div>
                        </div>
                    </div>
                    </div>
            </section>
        </React.Fragment>
    )
}

export default MainmodalCancel;
