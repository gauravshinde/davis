import React from 'react';

const MainmodaltwoOne = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal10">
                07PERSON PROFILE TEAM ADMIN-RESEND INVITE
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal10" role="dialog" aria-labelledby="exampleModalLabel10" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content ">
                        <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12 text-center">
                                <p>First Name Last Name</p>
                            </div>
                            <div className="col-8 m-auto">
                                <p className="modalTwoone-paragraph">You have sent First Name an invite to join your team on 02/06/2019. He is yet to log in.</p>
                            </div>
                            <div className="col-12 modalTwo-margin">
                                <button type="button" className="col-12 btn modalTwo-button border-top">Resend Invite</button>
                                <button type="button" className="col-12 btn modalTwo-button">Cancel Invitation</button>
                            </div>
                            <div className="col-12 text-center">
                                <button type="button" className="btn button-css modal-button-next">Done</button>
                            </div>
                        </div>
                    </div>
                    </div>
            </section>
        </React.Fragment>
    )
}

export default MainmodaltwoOne;
