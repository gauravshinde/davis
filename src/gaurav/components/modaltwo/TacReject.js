import React from 'react'

const TacReject = () => {
    return (
        <React.Fragment>
            <section>
            {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal17">
                08PERSON PROFILE TEAM User-TAC-Reject
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-lg" id="exampleModal17" role="dialog" aria-labelledby="exampleModalLabel17" aria-hidden="true">{/*tabindex="-1"*/}
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="col-12 modaltwo-close-button">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="col-12">
                            <p>Welcome to Soul.</p>
                        </div>
                        <div className="col-12">
                            <hr/>
                        </div>
                        <div className="col-8 m-auto">
                            <p className="tacreject-paragraph">You seem to be unhappy with the terms laid out for use of this service. Please let us know why and we can make our legal team aware of your grievences and if we can address them. Your Team Admin shall be notified of your objections. In case your objections are addressed, ask your admin to add you to the Index site again as a new user.</p>
                        </div>
                        <div className="col-12">
                            <hr/>
                        </div>
                        <div className="col-8 m-auto modal-height">
                            <p className="tacreject-paragraph">Write your objections here.</p>
                        </div>
                        <div className="col-11 m-auto">
                            <button type="button" className="btn button-css modalbottom-next pull-left" data-dismiss="modal">Back</button>
                            <button type="button" className="btn button-css modalbottom-next pull-right">Send</button>
                        </div>
                    </div>
                </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default TacReject;
