import React from 'react'

const TacRejectTwo = () => {
    return (
        <React.Fragment>
            <section>
            {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal18">
                08PERSON PROFILE TEAM User-TAC-Reject
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-lg" id="exampleModal18" role="dialog" aria-labelledby="exampleModalLabel18" aria-hidden="true">{/*tabindex="-1"*/}
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="col-12 modaltwo-close-button">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="col-12">
                            <p>Thank you for letting us know.</p>
                        </div>
                        <div className="col-12">
                            <hr/>
                        </div>
                        <div className="col-8 m-auto">
                            <p className="tacreject-paragraph">We strive to be better at this. Someone from our legal team will review your objections and let us know. Your team admin has also been notified so that the billing cycles can be reset.</p>
                        </div>
                        <div className="col-12">
                            <hr/>
                        </div>
                        <div className="col-12 text-center margin-topcls">
                            <button type="button" className="btn button-css modalbottom-next">Send</button>
                        </div>
                    </div>
                </div>
                </div>
            </section>
        </React.Fragment>
    )
}

export default TacRejectTwo;
