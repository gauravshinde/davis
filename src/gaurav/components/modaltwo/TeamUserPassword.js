import React from 'react'

const TeamUserPassword = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal19">
                09PERSON PROFILE TEAM User-password
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal19" role="dialog" aria-labelledby="exampleModalLabel19" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content ">
                        <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>Welcome to Soul</p>
                            </div>
                            <div className="col-12">
                                <hr className="mt-0 mb-0"/>
                                <p className="team-user-paragraph">Your organisation has added you@email.com to the davis index. to login set password.</p>
                                <hr className="mt-0 mb-0"/>
                            </div>
                            <div className="col-12">
                                <form>
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item teamuser-padding"><input type="text" className="form-control border-teamuser" placeholder="you@awesome.com" /> </li>
                                        <li className="list-group-item teamuser-padding"><input type="password" className="form-control border-teamuser" placeholder="Password" /> </li>
                                        <li className="list-group-item teamuser-padding"><input type="password" className="form-control border-teamuser" placeholder="Confirm password" /> </li>
                                    </ul>
                                </form>
                            </div>
                            <div className="col-12 text-center team-top-button">
                                <button type="button" className="btn button-css modal-button-next">Next</button>
                            </div>
                        </div>
                    </div>
                    </div>
            </section>
        </React.Fragment>
    )
}

export default TeamUserPassword;
