import React from 'react'

const WelcomLogin = () => {
    return (
        <React.Fragment>
            <section>
                {/* <!-- Button trigger modal --> */}
                <button type="button" className="btn btn-primary mt-4" data-toggle="modal" data-target="#exampleModal21">
                09PERSON PROFILE TEAM User-password
                </button>

                {/* <!-- Modal --> */}
                <div className="modal fade bd-example-modal-sm" id="exampleModal21" role="dialog" aria-labelledby="exampleModalLabel21" aria-hidden="true">{/*tabindex="-1"*/}
                    <div className="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div className="modal-content ">
                        <div className="col-12 modal-close-button">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="col-12">
                                <p>Welcome to Soul</p>
                            </div>
                            <div className="col-12">
                                <form>
                                    <div className="form-group margin-top-welcome">
                                        <input type="text" name="" id="" className="form-control border-top-form" placeholder="Email Address"/>
                                        <input type="password" name="" id="" className="form-control border-top-form" placeholder="Password"/>
                                    </div>
                                    <div className="col-12 text-center">
                                        <button type="button" className="btn button-css modal-button-next">Login</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>
            </section>
        </React.Fragment>
    )
}

export default WelcomLogin;
