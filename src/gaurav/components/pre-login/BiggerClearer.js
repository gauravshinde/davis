import React from 'react';

const BiggerClearer = () => {
  return (
    <React.Fragment>
        <section className="main-padding">
            <div className="container-fluid bigger-clearer-main">
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 bigger-clearer-main-divi">
                        <h3>The bigger, clearer picture</h3>
                        <p>Soul provides the most holistic view of the metals industry with proprietary price assesments and round-the-clock monitoring, we also provide reports compariosn and news from within the industry.</p>
                    </div>    
                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 bigger-clearer-main-division">
                    </div>    
                </div>
            </div>
        </section>
    </React.Fragment>
  )
}

export default BiggerClearer;
