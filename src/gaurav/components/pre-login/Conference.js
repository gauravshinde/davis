import React from 'react';

const Conference = () => {
  return (
    <React.Fragment>
        <section className="conference-main">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
                        <h3>Soul will be at the<br/>International Scrap<br/>Conference in Houston on<br/>June 21st 2019</h3>
                    </div>    
                    <div className="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7 conference-main-divisiontwo">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <button type="button" className="btn float-right button-css mt-4">Register</button>
                    </div>    
                </div>
            </div>
        </section>
    </React.Fragment>
  )
}

export default Conference;
