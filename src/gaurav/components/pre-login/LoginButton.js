import React from 'react';
import { Link } from "react-router-dom";

const LoginButton = () => {
  return (
    <React.Fragment>
        <div className="container-fluid main-padding">
            <div className="login-button-main">
                <Link to="/" className="button-login">Sign Up</Link>
            </div>
        </div>
    </React.Fragment>
  )
}

export default LoginButton;
