import React from 'react';
import RegisterNow from './RegisterNow';
import Conference from './Conference';
import SoulPublications from './SoulPublications';
import BiggerClearer from './BiggerClearer';
import LoginButton from './LoginButton';
import Signup from './Signup';

const PreMain = () => {
  return (
    <React.Fragment>
        <section>
          <LoginButton/>
          <Signup/>
          <BiggerClearer/>
          <SoulPublications/>
          <Conference/>
          <RegisterNow/>
        </section>
    </React.Fragment>
  )
}

export default PreMain;
