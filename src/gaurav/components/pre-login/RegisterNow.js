import React from 'react'

const RegisterNow = () => {
  return (
    <React.Fragment>
        <section className="main-padding">
          <div className="container register-pre-main">
              <div className="row">
                  <h3 className="col-12 text-center">Register Now to avail benefits.</h3>
                  <p className="col-12 col-sm-12 col-md-10 m-auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit,</p>
              </div>
              <div className="row mb-4">
                <button type="button" className="btn button-css m-auto">Buy Now</button>  
              </div>
          </div>
        </section>
    </React.Fragment>
  )
}

export default RegisterNow;
