import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class Signup extends Component {

    state = {
        email: '',
        password: ''
    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.history.push('/maindashboard');
    }

    render() {
        return (
            <React.Fragment>
                <div className="main-padding">
                    <div className="container-fluid signup-main">
                        <div className="row">
                            <div className="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 signup-maindivone">
                                <h3>Industry first, pay as-you-go pricing for a PRA tool for Ferrous and Non-Ferous metals.</h3>
                            </div>
                            <div className="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 signup-maindivtwo">
                                <div className="signup-form">
                                    <h5>Log In</h5>
                                    <form onSubmit={this.handleSubmit}>
                                        {/* <div className="form-group row">
                                            <div className="col-12">
                                                <input type="text" className="form-control signup-input" name="name" id="name" placeholder="Enter Name" />
                                            </div>
                                        </div> */}
                                        <div className="form-group row">
                                            <div className="col-12">
                                                <input type="email" className="form-control signup-input" name="email" id="email" placeholder="Enter Email Address" onChange={this.handleChange} required/>
                                            </div>
                                        </div>
                                        <div className="form-group row">
                                            <div className="col-12">
                                                <input type="password" className="form-control signup-input" name="password" id="password" placeholder="***********"  onChange={this.handleChange} required/>
                                            </div>
                                        </div>
                                        {/* <div className="form-group row">
                                            <div className="col-12">
                                                <input type="password" className="form-control signup-input" name="confirmpass" id="confirmpass" placeholder="***********" />
                                            </div>
                                        </div> */}
                                        <div className="col-12 text-center">
                                            <button type="submit" className="btn button-css button-signup">Log In</button>
                                        </div>
                                    </form>
                                </div>                    
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default withRouter(Signup);

