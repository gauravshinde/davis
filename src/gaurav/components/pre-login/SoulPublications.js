import React from 'react';
import Soulpublications from '../../json/pre-login/Soulpublication.json';

const SoulPublications = () => {
  return (
    <React.Fragment>
        <section className="main-padding">
            <div className="container-fluid soul-publication-main">
                <h3>Soul Publications</h3>
                <div className="container">
                    <div className="row">
                        {Soulpublications.map((soul, index) => {
                            return <div key={soul._id} className="col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                        <div className="col-12 box-division">
                                            <h3>{soul.soul_title}</h3>
                                        </div>
                                        <p>{soul.soul_para}</p>
                                        <div className="col-12 text-center">
                                            <button type="button" className="btn button-css">Buy</button>
                                        </div>
                                    </div> 
                        })}
                    </div>
                </div>
            </div>
        </section>
    </React.Fragment>
  )
}

export default SoulPublications;
