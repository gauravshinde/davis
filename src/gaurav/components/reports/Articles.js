import React from 'react';
import Reports from './Reports';


const Articles = () => {
    return (
        <React.Fragment>
            <div className="container-fluid main-padding">
                <div className="row">
                    <Reports
                        title={"Reports"}
                    />
                    <Reports
                        title={"Notices"}
                    />
                    <Reports
                        title={"Articles"}
                    />
                </div>
            </div>
        </React.Fragment>
    )
}

export default Articles;
