import React from 'react';
// import Button from 'react-bootstrap/Button';

const Reports = ({
    title,
    paragraph
}) => {
    return (
        <React.Fragment>
            <div className="col-12 border mt-2">
                <h5 className="card-title">{title}</h5>
                <p className="card-text">{paragraph}</p>
            </div>
        </React.Fragment>
    )
}

export default Reports;
