import React from 'react';
import { Link } from "react-router-dom";
import Eventsj from '../../json/resources/Events';

const Events = () => {
    return (
        <React.Fragment>
            <section className="main-padding">
                <div className="container-fluid events-main">
                    {Eventsj.map((event, index) => {
                        return  <div className="row" key={event._id}>
                                    <div className="col-9 col-sm-9 col-md-9 col-lg-9 col-xl-9">
                                        <h4>{event.event_title}</h4>
                                        <p className="events-para">{event.event_para}</p>
                                    </div>
                                    <div className="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right align-self-end">
                                        <span>{event.event_location}</span><br/>
                                        <span>{event.event_date} | {event.event_time} Local time</span>
                                    </div>
                                    <div className="col-12">
                                        <hr/>
                                    </div>
                                </div>
                    })}
                    <ul className="pagination">
                        <li className="page-item"><Link className="page-link" to={""}>1</Link></li>
                        <li className="page-item"><Link className="page-link" to={""}>2</Link></li>
                        <li className="page-item"><Link className="page-link" to={""}>3</Link></li>
                        <li className="page-item"><Link className="page-link" to={""}>4</Link></li>
                        <li className="page-item"><Link className="page-link" to={""}>5</Link></li>
                    </ul>
                </div>

            </section>
        </React.Fragment>
    )
}

export default Events;
